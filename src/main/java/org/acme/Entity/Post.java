package org.acme.Entity;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;

import io.quarkus.hibernate.orm.panache.PanacheEntity;


@Entity
public class Post extends PanacheEntity{

    
    @Column(name = "title")
    public String title;

    
    @Column(name = "content")
    public String content;

    @ManyToMany @JoinColumn
    public List<Tags> tags;

        
}
