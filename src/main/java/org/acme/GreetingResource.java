package org.acme;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.acme.Entity.Post;
import org.acme.Entity.Tags;
import org.jboss.logging.annotations.Param;

import io.quarkus.narayana.jta.runtime.TransactionConfiguration;
import io.vertx.codegen.doc.Tag;


@Path("/api")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GreetingResource {

    
    // post CRUD
    @GET
    @Path("post")
    public Response getAllPost() {
        List<Post> post =Post.listAll();
        return Response.ok(post).build();
    }

    //get post by id
    @GET
    @Path("post/id/{id}")
    public Response getById(@PathParam ("id") Long id){
       return Post.findByIdOptional(id)
              .map(post -> Response.ok(post).build())
              .orElse(Response.status(Status.NOT_FOUND).build());
    }

    //put post by id
    @PUT
    @Path("post/update/{id}")
    @Transactional
    public Response updatePost (@PathParam("id") Long id, Post post) {
        Post postUpdate = Post.findById(id);
       
        if(postUpdate == null) {
            Response.status(Status.NOT_FOUND).build();
        }

        postUpdate.id = post.id;
        postUpdate.content = post.content;
        postUpdate.title = post.title;
        postUpdate.tags=postUpdate.tags;


        return Response.status(Status.OK).build();
    }

    //get post by title
    @GET
    @Path("post/title/{title}")
    public Response getByTitle(@PathParam("title") String title){
        return Post
            .find("title", title)
            .singleResultOptional()
            .map(post -> Response.ok(post).build())
            .orElse(Response.status(Status.NOT_FOUND).build());
    }

    //create new post
    @POST
    @Path("post/add")
    @Transactional
    public Response createPost (Post post){
        Post.persist(post);

        if(post.isPersistent()){

            return Response.created(URI.create("/posts/" + post.id)).build();
        
        }

        return Response.status(Status.NOT_FOUND).build();

    }
    
    // delete post by id
    @DELETE
    @Path("post/delete/{id}")
    @Transactional
    public Response deleteMovieById(@PathParam("id") Long id){
        boolean deleted = Post.deleteById(id);
        return deleted ? Response.noContent().build() : Response.status(Status.BAD_REQUEST).build();
    }


    // tags CRUD
    // get tag all
    @GET
    @Path("tags")
    public Response getAllTags() {
        List<Tags> tags =Tags.listAll();
        return Response.ok(tags).build();
    }

    
    // get tags by id
    @GET
    @Path("tags/id/{id}")
    public Response getTagsById(@PathParam ("id") Long id){
       return Tags.findByIdOptional(id)
              .map(post -> Response.ok(post).build())
              .orElse(Response.status(Status.NOT_FOUND).build());
    }

    @PUT
    @Path("tags/update/{id}")
    @Transactional
    public Response updateTags (@PathParam("id") Long id, Tags tags) {
        Tags tagsUpdate = Tags.findById(id);
       
        if(tagsUpdate == null) {
            Response.status(Status.NOT_FOUND).build();
        }

        tagsUpdate.id = tags.id;
        tagsUpdate.label = tags.label;
        tagsUpdate.post = tags.post;

        return Response.status(Status.OK).build();
    }

    // get tags by label
    @GET
    @Path("tags/label/{label}")
    public Response getByLabel(@PathParam("label") String label){
        return Post
            .find("label", label)
            .singleResultOptional()
            .map(tags -> Response.ok(tags).build())
            .orElse(Response.status(Status.NOT_FOUND).build());
    }

     //create new post
     @POST
     @Path("tags/add")
     @Transactional
     public Response createTags (Tags tags){
         Tags.persist(tags);
 
         if(tags.isPersistent()){
 
             return Response.created(URI.create("/posts/" + tags.id)).build();
         
         }
 
         return Response.status(Status.NOT_FOUND).build();
 
     }

    // delete tag by id
    @DELETE
    @Path("tags/delete/{id}")
    @Transactional
    public Response deleteTagsById(@PathParam("id") Long id){
        boolean deleted = Post.deleteById(id);
        
        if(deleted){
            return Response.noContent().build();
        }
        
        return  Response.status(Status.BAD_REQUEST).build();
    }


}